# data-science-stak

## Stack composition

* [Jupyter notebook server](https://jupyter.org/)
* [MSSQL](https://www.microsoft.com/en-us/sql-server/sql-server-2017) v 2017
* prepopulated [Postgresql](https://www.postgresql.org/): [https://hub.docker.com/repository/docker/zar3bski/populated_postgres](https://hub.docker.com/repository/docker/zar3bski/populated_postgres)

## Credentials

| component       | user        | password         | 
|----------------:|:-----------:|:----------------:|
| **jupyter**     |             | `some_password`  |
| **postgres**    |`s0m3us3r`   |`s0m3password`    |
| **mssql**       | `sa`        |`pt2hNazoSzMFpE3M`|